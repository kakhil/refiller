package com.superbia.superbia.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.superbia.superbia.R;
import com.superbia.superbia.others.GPSTracker;
import com.superbia.superbia.others.HelperObj;
import com.superbia.superbia.others.PlaceArrayAdapter;
import com.superbia.superbia.others.Values;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;


public class FragLocations extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks {
    private Context context;
    private FragmentManager fragmentManager;
    private GoogleMap googleMap;
    private GoogleApiClient mGoogleApiClient;
    private LatLng latLngOrg,DropLatLng;
    private Marker currLocationMarker;
    private  OnMapReadyCallback onMapReadyCallback;
    private ArrayList<Double> latList;
    private ArrayList<Double> Lonlist;
    private ArrayList<String> TypeList;
    private ArrayList<String> NameList;
    private Button btnFountain,btnCoffee,btnWater;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private AutoCompleteTextView etSearch;
    private LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private JSONArray jsonArrayData=null;
    private AssetManager assetManager;
    private InputStream is;
    private Bitmap bitmap;
    private JSONObject MainJSon=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_frag_locations, container, false);
        SetUp(view);
        onClickkEvents();
        return view;
    }
 public void SetUp(View view){
    try {
        context=getActivity();
        fragmentManager=getFragmentManager();
        onMapReadyCallback = this;
        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map1);
        mapFrag.getMapAsync(onMapReadyCallback);
        btnFountain=(Button)view.findViewById(R.id.btnFountain);
        etSearch=(AutoCompleteTextView)view.findViewById(R.id.etSearch);
        btnCoffee=(Button)view.findViewById(R.id.btnCoffee);
        btnWater=(Button)view.findViewById(R.id.btnWater);
        //
        etSearch.setThreshold(2);
        etSearch.setOnItemClickListener(mAutocompleteClickListener);
        mPlaceArrayAdapter = new PlaceArrayAdapter(context, R.layout.list_item_spinner_map,
                BOUNDS_MOUNTAIN_VIEW, null);
        etSearch.setAdapter(mPlaceArrayAdapter);
        //
        latList=new ArrayList<>();
        Lonlist=new ArrayList<>();
        TypeList=new ArrayList<>();
        NameList=new ArrayList<>();
        //
        CalJson();
        //
        if(jsonArrayData!=null&&jsonArrayData.length()>0){
            for(int i=0;i<jsonArrayData.length();i++){
                latList.add(jsonArrayData.getJSONObject(i).optDouble("Latitude"));
                Lonlist.add(jsonArrayData.getJSONObject(i).optDouble("Longitude"));
                TypeList.add(jsonArrayData.getJSONObject(i).optString("Type"));
                NameList.add(jsonArrayData.getJSONObject(i).optString("Name"));
            }
        }
        //
        //setCurrentLocation();
    } catch (Exception e) {
        e.printStackTrace();
    }
}
    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        try {
            if (googleMap != null) {
                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    googleMap.setMyLocationEnabled(false);
                }
                buildGoogleApiClient();
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    protected synchronized void buildGoogleApiClient() {
        try {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .build();
            mGoogleApiClient.connect();
            //
            onGPS();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CalJson() {
        try {
            assetManager = getActivity().getAssets();
            MainJSon=new JSONObject(loadJSONFromAsset());
            jsonArrayData=MainJSon.optJSONArray("SydneyLocations");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String loadJSONFromAsset() {
        String json = null;
        try {
            is = getActivity().getAssets().open("list.txt");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return json;
    }


    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final PlaceArrayAdapter.PlaceAutocomplete name = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);

           // Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
           // Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);

        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
//                Log.e(LOG_TAG, "Place query did not complete. Error: " +
//                        places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();
//            latLngOrg = new LatLng(place.getLatLng().latitude,place.getLatLng().longitude);
//            setMarker(latLngOrg);
//            latd=place.getLatLng().latitude;
//            double latd1=place.getLatLng().longitude;
//            // HelperObj.getInstance().cusToast(ActivitySourceLoaction.this, String.valueOf(latd));
//            if(latd>0){
//
//                //finish();
//            }else {
//                HelperObj.getInstance().cusToast(context, "Not Found");
//            }

            if (attributions != null) {
                // mAttTextView.setText(Html.fromHtml(attributions.toString()));
            }
        }
    };
    protected void setCurrentLocation() {
        try {
                GPSTracker gps = new GPSTracker(getActivity());
                if(gps.canGetLocation()) {
                    double latitudeCurrent = gps.getLatitude();
                    double longitudeCurrent = gps.getLongitude();
                    if(latitudeCurrent > 0 && longitudeCurrent > 0){
                        latLngOrg = new LatLng(latitudeCurrent, longitudeCurrent);
                        setMarker(latLngOrg);
                    }
                } else {
                    latLngOrg = new LatLng(Values.latitude, Values.longitude);
                    setMarker(latLngOrg);
                }

            etSearch.setText("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void onClickkEvents() {
        btnFountain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleMap.clear();
                for(int i = 0 ; i < latList.size() ; i++) {
                    createMarker(TypeList.get(i),latList.get(i), Lonlist.get(i),"Fountain",NameList.get(i));
                }
            }
        });
        btnWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleMap.clear();
                for(int i = 0 ; i < latList.size() ; i++) {
                    createMarker(TypeList.get(i),latList.get(i), Lonlist.get(i),"Tap", NameList.get(i));
                }
            }
        });
        btnCoffee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleMap.clear();
                for(int i = 0 ; i < latList.size() ; i++) {
                    createMarker(TypeList.get(i),latList.get(i), Lonlist.get(i),"Coffee", NameList.get(i));
                }
            }
        });
    }


    protected Marker createMarker(String desc, Double lat, Double lon, String condition, String s) {
        Marker marker = null;
        if(condition.equalsIgnoreCase("Coffee")){
            if(desc.equals("Coffee")){
                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lon))
                        .anchor(0.4f, 0.4f)
                        .title(s)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.cafe)));
            }

        }else if(condition.equalsIgnoreCase("Tap")){
            if(desc.equals("Tap")){

                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lon))
                        .anchor(0.4f, 0.4f)
                        .title(s)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.water)));
            }
        }else if(condition.equalsIgnoreCase("Fountain")){
            if(desc.equals("Fountain")){

                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lon))
                        .anchor(0.4f, 0.4f)
                        .title(s)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.fountain)));
            }

        }else {

            if(desc.equals("Coffee")){
                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lon))
                        .anchor(0.4f, 0.4f)
                        .title(s)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.cafe)));
            }else  if(desc.equals("Tap")){

                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lon))
                        .anchor(0.4f, 0.4f)
                        .title(s)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.water)));
            }else  if(desc.equals("Fountain")){
                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lon))
                        .anchor(0.4f, 0.4f)
                        .title(s)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.fountain)));
            }
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLngOrg).zoom(11).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        }

        return marker;
    }
    @Override
    public void onConnected(Bundle bundle) {
        try {
            Location mLastLocation = null;
            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);
            }
            if (mLastLocation != null) {
                //place marker at current position
                googleMap.clear();
                //latLngOrg = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            } else {
                //latLngOrg = new LatLng(Values.latitude, Values.longitude);
            }
            // mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);

            setCurrentLocation();
            setMarker(latLngOrg);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLngOrg).zoom(11).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onConnectionSuspended(int i) {
        try {
            // mPlaceArrayAdapter.setGoogleApiClient(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  void setMarker(LatLng latLng){
        try {
            double latitudeOrg=0,longitudeOrg=0;
            latitudeOrg = latLng.latitude;
            longitudeOrg = latLng.longitude;
            if(latitudeOrg > 0 && longitudeOrg > 0 ){
                googleMap.clear();
                //String cityName = HelperObj.getInstance().getLocationName(context, latitudeOrg, longitudeOrg);
                String cityName = HelperObj.getInstance().getLocationName(context, latitudeOrg, longitudeOrg);
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(cityName);
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                currLocationMarker = googleMap.addMarker(markerOptions);
                //
                latLngOrg = latLng;
                float zoomLevel = googleMap.getCameraPosition().zoom;
                if(zoomLevel < 14){
                    zoomLevel = 14;
                }
                //

                for(int i = 0 ; i < latList.size() ; i++) {
                    createMarker(TypeList.get(i),latList.get(i), Lonlist.get(i),"All", NameList.get(i));
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latLngOrg).zoom(zoomLevel).build();
                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));
                }
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLngOrg).zoom(zoomLevel).build();
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            } else {
                HelperObj.getInstance().cusToast(context,"Lat,Lng not found. Select valid location");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onGPS(){
        LocationRequest mLocationRequest = new LocationRequest();
        // Check the location settings of the user and create the callback to react to the different possibilities
        LocationSettingsRequest.Builder locationSettingsRequestBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, locationSettingsRequestBuilder.build());
        result.setResultCallback(mResultCallbackFromSettings);
    }
    // The callback for the management of the user settings regarding location
    private ResultCallback<LocationSettingsResult> mResultCallbackFromSettings = new ResultCallback<LocationSettingsResult>() {
        @Override
        public void onResult(LocationSettingsResult result) {
            final Status status = result.getStatus();
            //final LocationSettingsStates locationSettingsStates = result.getLocationSettingsStates();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(getActivity(),0);
                    } catch (Exception e) {
                        // Ignore the error.
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Log.e("TAG", "Settings change unavailable. We have no way to fix the settings so we won't show the dialog.");
                    break;
            }
        }
    };
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(intent);
        switch (requestCode) {
            case 0:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        if (mGoogleApiClient.isConnected()/* && userMarker == null*/) {
                            // HelperObj.getInstance().Loader(context,fragmentManager,true,"Getting location...");
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        // HelperObj.getInstance().Loader(context,fragmentManager,false,null);
                                       // setCurrentLocation();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, 5000);
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        break;
                    default:
                        break;
                }
                break;
        }
    }

}

