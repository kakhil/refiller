package com.superbia.superbia.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.superbia.superbia.R;
import com.superbia.superbia.database.NetworkCheck;
import com.superbia.superbia.others.HelperObj;


public class DialogNetworkError extends DialogFragment {
    Context context;
    Button btnOK;

    //---empty constructor required
    public DialogNetworkError() {
    }
    public void setDialogTitle(Context ctx) {
        context = ctx;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar);
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        View view = inflater.inflate(R.layout.dialog_network_error, container);
        //
        btnOK = (Button) view.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NetworkCheck networkCheck=new NetworkCheck();
                if(networkCheck.isConnected(context)){
                    dismiss();
                    if(HelperObj.whichNetworkError.equals("ActivitySplash")){
                        //HelperObj.getInstance().getActivitySplash().reloadGetDeviceDetails();
                    } else if(HelperObj.isWhichActivity.equals("ActivityLocationChangeGPS")){
                        //HelperObj.getInstance().getActivityLocationChange().checkLocation();
                    } else if(HelperObj.whichWebServiceRequest.equals("ActivityMainGetHomeScreenList")){
                        //HelperObj.getInstance().getFragHome().getHomeScreenData("","FragHome");
                        //HelperObj.getInstance().getActivityMain().getCartCount();
                    } else if(HelperObj.whichWebServiceRequest.equals("ActivitySplashGetHomeScreenList")){
                        //HelperObj.getInstance().getActivitySplash().getHomeScreenData("","ActivitySplash");
                    } else if(HelperObj.whichWebServiceRequest.equals("ActivityLocationChangeGetHomeScreenList")){
                        //HelperObj.getInstance().getActivityLocationChange().getHomeScreenData("","ActivityLocationChange");
                    }
                }else {
                    HelperObj.getInstance().cusToast(context,"No Internet Connection");
                }
            }
        });
        //
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        return view;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        HelperObj.dialogNetworkError = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        HelperObj.dialogNetworkError = null;
    }
}
