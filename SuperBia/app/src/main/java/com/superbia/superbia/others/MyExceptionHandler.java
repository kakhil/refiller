package com.superbia.superbia.others;

import android.app.Activity;
import android.app.PendingIntent;
import android.util.Log;




public class MyExceptionHandler implements Thread.UncaughtExceptionHandler {
    private final Activity myActivity;
    private final Class<?> myActivityClass;
    PendingIntent pendingIntent;

    public MyExceptionHandler(Activity activity, Class<?> c) {
        myActivity = activity;
        myActivityClass = c;
    }
    public void uncaughtException(Thread thread, final Throwable exception) {
        try {
            Log.v("MyVerbose",exception.getMessage() == null ? "" : exception.getMessage());
            Log.d("MyDebug",exception.getMessage() == null ? "" : exception.getMessage());
            /*Intent intent = new Intent(myActivity, ActivityExceptionError.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("data",exception.getMessage());
            PendingIntent pendingIntent = PendingIntent.getActivity(MyApplication.getIntance().getBaseContext(), 0, intent, 0);
            //Following code will restart your application after 2 seconds
            AlarmManager mgr = (AlarmManager) MyApplication.getIntance().getBaseContext()
                    .getSystemService(Context.ALARM_SERVICE);
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000,
                    pendingIntent);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            //This will finish your activity manually
            //myActivity.finish();
            //This will stop your application and take out from it.
            //System.exit(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}