package com.superbia.superbia;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.superbia.superbia.others.UserSession;
import com.superbia.superbia.others.Values;

public class ActvitySplash1 extends FragmentActivity {
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actvity_splash1);
        context=getApplicationContext();
        UserSession.getInstance(context).getUserDetails() ;

        try {
            Handler splash_time_handler = new Handler(Looper.getMainLooper());
            splash_time_handler.postDelayed(new Runnable() {
                public void run() {
                    if(Values.UserEmail.isEmpty()){
                        Intent i=new Intent(context,ActivitySplash.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }else {
                        Intent i=new Intent(context,MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }//run()
            }, 3000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
