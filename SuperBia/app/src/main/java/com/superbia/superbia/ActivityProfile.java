package com.superbia.superbia;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superbia.superbia.others.HelperObj;
import com.superbia.superbia.others.UserSession;
import com.superbia.superbia.others.Values;

import org.json.JSONArray;

public class ActivityProfile extends FragmentActivity {
    private Context context;
    private ImageView ib_back;
    private FragmentManager fragmentManager;
    private JSONArray jsonArrayData=null;
    private EditText etUsername,etMobileNumber,etPassword,etEmailAddress,etConfirmPassword;
    private Button btnSignUP;
    private String PhoneNunmber="",Password="",Username="",EmailAddress="",ConfirmPassword="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        context=getApplicationContext();
        fragmentManager=getSupportFragmentManager();
        etUsername=(EditText)findViewById(R.id.etUsername);
        etMobileNumber=(EditText)findViewById(R.id.etMobileNumber);
        etEmailAddress=(EditText)findViewById(R.id.etEmailAddress);
        UserSession.getInstance(context).getUserDetails();

        btnSignUP=(Button)findViewById(R.id.btnSignUP);
        etUsername.setText(Values.UserFullName);
        etMobileNumber.setText(Values.UserNumber);
        etEmailAddress.setText(Values.UserEmail);
        btnSignUP.setVisibility(View.GONE);
        //
        ib_back=(ImageView)findViewById(R.id.ib_back);
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnSignUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Username=etUsername.getText().toString();
                EmailAddress=etEmailAddress.getText().toString();
                PhoneNunmber=etMobileNumber.getText().toString();
                //
                if(Username.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter UserName.");
                } else if(PhoneNunmber.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter mobile number.");
                }else if(EmailAddress.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter Email.");
                } else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(EmailAddress).matches()){
                    HelperObj.getInstance().cusToast(context,"Please enter valid Email.");
                } else {
                    HelperObj.getInstance().cusToast(context,"Successfully Updated.");
                }
            }
        });
    }
}
