package com.superbia.superbia.others;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;


public class UserSession {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private static String KEY_UserID = "KEY_UserID";
    private static String KEY_UserNameID = "KEY_UserNameID";
    private static String KEY_Email = "KEY_Email";
    private static String KEY_Number = "KEY_Number";
    private static String KEY_FullName = "KEY_FullName";
    private static String KEY_UserImage = "KEY_UserImage";
    private static String KEY_DOB = "KEY_DOB";
    private static String KEY_Gender = "KEY_Gender";
    private static String KEY_FCM_Token = "KEY_FCM_Token";

    @SuppressLint("StaticFieldLeak")
    private static UserSession objInstance;
    public static UserSession getInstance(Context context) {
        if (objInstance == null) {
            objInstance = new UserSession(context);
        }
        return objInstance;
    }
    @SuppressLint("CommitPrefEdits")
    private UserSession(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        editor = pref.edit();
    }
    public void createUserSession(String FullName, String email, String mobileNo) {
        editor.putString(KEY_FullName, HelperObj.getInstance().IsNullReturnValue(FullName,""));
        editor.putString(KEY_Email, HelperObj.getInstance().IsNullReturnValue(email,""));
        editor.putString(KEY_Number, HelperObj.getInstance().IsNullReturnValue(mobileNo,""));
        editor.commit();
        //
        getUserDetails();
    }
    public void CreateFCMToken(String token){
        editor.putString(KEY_FCM_Token, HelperObj.getInstance().IsNullReturnValue(token,""));
        editor.commit();
    }
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_FullName, pref.getString(KEY_FullName, ""));
        user.put(KEY_Email, pref.getString(KEY_Email, ""));
        user.put(KEY_Number, pref.getString(KEY_Number, ""));
        //
        Values.UserFullName = HelperObj.getInstance().IsNullReturnValue(user.get(UserSession.KEY_FullName),"");
        Values.UserNumber = HelperObj.getInstance().IsNullReturnValue(user.get(UserSession.KEY_Number),"");
        Values.UserEmail = HelperObj.getInstance().IsNullReturnValue(user.get(UserSession.KEY_Email),"");
        //
        return user;
    }
    public void logoutUser() {

        editor.remove(KEY_FullName);
        editor.remove(KEY_Number);
        editor.remove(KEY_Email);
        editor.clear();
        editor.commit();
        //
        Values.UserFullName = "";
        Values.UserNumber = "";
        Values.UserEmail = "";
    }
    public Integer getUserId() {
        return pref.getInt(KEY_UserID, 0);
    }
    public boolean isUserLoggedIn() {
        if(Values.UserID > 0 && !Values.UserNameID.isEmpty()){
            return true;
        } else {
            return false;
        }
    }
}
