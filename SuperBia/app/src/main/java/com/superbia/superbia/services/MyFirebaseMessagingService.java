package com.superbia.superbia.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.superbia.superbia.ActivitySplash;
import com.superbia.superbia.R;
import com.superbia.superbia.database.DataBase;

import org.json.JSONObject;

import java.util.Date;
import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> params = remoteMessage.getData();
            Context context = getApplicationContext();
            JSONObject finalJsonObject = new JSONObject(params);
            JSONObject jsonObject = new JSONObject(finalJsonObject.optString("message"));
            DataBase dataBase = new DataBase(context);
            dataBase.open();
            dataBase.CreateChat((int)System.currentTimeMillis(),jsonObject.optString("text"),finalJsonObject.optString("message"),finalJsonObject.optString("message"),"",jsonObject.optString("date"));
            dataBase.close();
            //
            sendNotification(jsonObject.optString("text"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendNotification(String message) {
        try {
            Intent intent = new Intent(this, ActivitySplash.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("isNotificationActivity","Yes");
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getApplicationContext().getResources().getString(R.string.app_name))
                    //.setSubText("Sub Message")
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}