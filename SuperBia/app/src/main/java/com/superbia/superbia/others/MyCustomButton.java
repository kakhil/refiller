package com.superbia.superbia.others;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;


@SuppressLint("AppCompatCustomView")
public class MyCustomButton extends Button {

    public MyCustomButton(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public MyCustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public MyCustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface(Values.fontPath, context);
        setTypeface(customFont);
    }
}
