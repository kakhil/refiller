package com.superbia.superbia;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.superbia.superbia.others.HelperObj;

public class ActivitFeedBack extends AppCompatActivity {

    private ImageView ib_back;
    private Context context;
    private EditText et_Message;
    private Button btnSend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activit_feed_back);
        context=getApplicationContext();
        ib_back=(ImageView)findViewById(R.id.ib_back);
        et_Message=(EditText)findViewById(R.id.et_Message);
        btnSend=(Button)findViewById(R.id.btnSend);

        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message=et_Message.getText().toString();
                if(message.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter message");
                }else {
                    et_Message.setText("");
                    HelperObj.getInstance().cusToast(context,"Thanks for your FeedBack");
                    onBackPressed();
                }
            }
        });

    }
}


