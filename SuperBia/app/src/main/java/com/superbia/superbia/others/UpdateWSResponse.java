package com.superbia.superbia.others;

import org.json.JSONObject;


public interface UpdateWSResponse {
    void updateWSResponse(JSONObject response, String requestType);
}