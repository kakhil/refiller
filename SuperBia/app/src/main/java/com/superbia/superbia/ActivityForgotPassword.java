package com.superbia.superbia;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.superbia.superbia.others.HelperObj;

public class ActivityForgotPassword extends FragmentActivity {
    private Context context;
    private FragmentManager fragmentManager;
    private EditText etEmail;
    private Button btnForgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        context=getApplicationContext();
        fragmentManager=getSupportFragmentManager();
        etEmail=(EditText)findViewById(R.id.etEmail);
        btnForgot=(Button)findViewById(R.id.btnForgot);
        //
        btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Email=etEmail.getText().toString();
                if(Email.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please Enter Email");

                }else {
                    Intent i=new Intent(context,ActivityLogin.class);
                    startActivity(i);
                }
            }
        });
    }
}
