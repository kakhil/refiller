package com.superbia.superbia.dialogs;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.superbia.superbia.R;
import com.superbia.superbia.others.HelperObj;


public class DialogAirPlaneMode extends DialogFragment {
    Context context;
    Button btnOK;

    //---empty constructor required
    public DialogAirPlaneMode() {
    }
    public void setDialogTitle(Context ctx) {
        context = ctx;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar);
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        View view = inflater.inflate(R.layout.dialog_flight_mode, container);
        //
        context=getActivity();
        btnOK = (Button) view.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    boolean state = isAirplaneMode(context);
                    if (state){
                     HelperObj.getInstance().cusToast(context,"Your in Flight Mode please check once");
                    }else {
                        dismiss();
                        if(HelperObj.whichNetworkError.equals("ActivitySplash")){
                            //HelperObj.getInstance().getActivitySplash().reloadGetDeviceDetails();
                        } else if(HelperObj.isWhichActivity.equals("ActivityLocationChangeGPS")){
                            //HelperObj.getInstance().getActivityLocationChange().checkLocation();
                        } else if(HelperObj.whichWebServiceRequest.equals("ActivityMainGetHomeScreenList")){
                           // HelperObj.getInstance().getFragHome().getHomeScreenData("","FragHome");
                            //HelperObj.getInstance().getActivityMain().getCartCount();
                        } else if(HelperObj.whichWebServiceRequest.equals("ActivitySplashGetHomeScreenList")){
                           //HelperObj.getInstance().getActivitySplash().getHomeScreenData("","ActivitySplash");
                        } else if(HelperObj.whichWebServiceRequest.equals("ActivityLocationChangeGetHomeScreenList")){
                            //HelperObj.getInstance().getActivityLocationChange().getHomeScreenData("","ActivityLocationChange");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        //
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        return view;
    }
    @SuppressLint("NewApi")
    public  boolean isAirplaneMode(Context context) {
        // check the version
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {// if
            // less
            // than
            // version
            // 4.2

            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;

        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        HelperObj.dialogAirPlaneMode = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        HelperObj.dialogAirPlaneMode = null;
    }
}
