package com.superbia.superbia;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superbia.superbia.database.DataBase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

public class ActivityNotications extends FragmentActivity {
    private Context context;
    private ImageView ib_back;
    private FragmentManager fragmentManager;
    private LinearLayout llList;
    private JSONArray jsonArrayData=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notications);
        context=getApplicationContext();
        fragmentManager=getSupportFragmentManager();
        llList=(LinearLayout) findViewById(R.id.llList);
        //
        ib_back=(ImageView)findViewById(R.id.ib_back);
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        CallDatabase();
    }
    public void CallDatabase() {
        try {
            DataBase dataBase=new DataBase(context);
            dataBase.open();
            jsonArrayData=dataBase.GetDataChatAll();
            if(jsonArrayData!=null &&jsonArrayData.length()>0){
                llList.setVisibility(View.VISIBLE);
                setList();
            }else {
                llList.setVisibility(View.GONE);

            }
            //dataBase.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void onClickEvents() {

    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    public View getFavoriteListItem(final Integer position, final JSONObject jsonObject){
        final TextView tv_Name;
        ImageView ivImage;
        View rowView = null;
        LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            rowView = inflater.inflate(R.layout.item_viewlist, null);
            tv_Name = (TextView) rowView.findViewById(R.id.tv_Name);

            tv_Name.setText(jsonObject.optString("Message"));
            //
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowView;
    }
    public void setList(){
        try {
            llList.setVisibility(View.VISIBLE);
            llList.removeAllViews();
            if(jsonArrayData != null && jsonArrayData.length() > 0){
                for(int i = 0;i < jsonArrayData.length();i++){
                    llList.addView(getFavoriteListItem(i,jsonArrayData.getJSONObject(i)));
                }
            } else {
                llList.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}

