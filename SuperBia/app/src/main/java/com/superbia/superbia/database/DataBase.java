package com.superbia.superbia.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.superbia.superbia.R;
import com.superbia.superbia.others.HelperObj;

import org.json.JSONArray;

import java.io.File;

/**
 * Created by AapsTech It Solutions on 27-Jan-16.
 */
public class DataBase {
    private final String DATABASE_NAME = "DataBase";
    private final int DATABASE_VERSION = 1;
    private DbHelper ourHelper;
    private Context ourContext;
    private SQLiteDatabase ourDatabase;
    private String STORAGE_PATH = "";
    private String ChatCreateQuery = "CREATE TABLE IF NOT EXISTS notifications ( " + "PrimID INTEGER PRIMARY KEY AUTOINCREMENT, MsgID INTEGER NOT NULL, Message NVARCHAR(5000) NOT NULL, MessageDesc NVARCHAR(5000) NOT NULL, MsgImage NVARCHAR(5000) NULL, MessageType NVARCHAR(50) NOT NULL, MessageDate NVARCHAR(100) NOT NULL );";

    private String UserCreateQuery = "CREATE TABLE IF NOT EXISTS UserTable (Username NVARCHAR(5000) NOT NULL, Email NVARCHAR(5000) NOT NULL, Mobile NVARCHAR(5000) NULL,Password NVARCHAR(100) NOT NULL);";

    private class DbHelper extends SQLiteOpenHelper {
        public DbHelper(Context context) {
            super(context, STORAGE_PATH+DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(ChatCreateQuery);
            db.execSQL(UserCreateQuery);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + "notifications");
            db.execSQL("DROP TABLE IF EXISTS " + "UserTable");
            onCreate(db);
            /*if (newVersion > oldVersion) {
                db.execSQL("ALTER TABLE foo ADD COLUMN new_column INTEGER DEFAULT 0");
            }*/
        }
    }
    public DataBase(Context c) {
        ourContext = c;
    }
    public DataBase open() throws SQLException {
        String appName = ourContext.getResources().getString(R.string.app_name);
        STORAGE_PATH = ourContext.getApplicationInfo().dataDir+"/"+appName+"/Database/";
        //STORAGE_PATH = Environment.getExternalStorageDirectory()+"/"+appName+"/Database/";
        File dir  = new File(STORAGE_PATH);
        if(!dir.exists() && !dir.isDirectory()) {
            dir.mkdirs();
        }
        ourHelper = new DbHelper(ourContext);
        ourDatabase = ourHelper.getWritableDatabase();
        return this;
    }
    public void close(){
        ourHelper.close();
    }
    //FOR notifications
    public long CreateChat(Integer MsgID,String Message,String MessageDesc,String MsgImage,String MessageType,String MessageDate) {
        ContentValues cv = new ContentValues();
        if(HelperObj.getInstance().IsNull(MsgImage)){
            MsgImage = "";
        }
        cv.put("MsgID", MsgID);
        cv.put("Message", Message);
        cv.put("MessageDesc", MessageDesc);
        cv.put("MsgImage", MsgImage);
        cv.put("MessageType", MessageType);
        cv.put("MessageDate", MessageDate);
        if(!CheckMessageExist(MsgID,MessageDate)){
            return ourDatabase.insert("notifications", null, cv);
        } else {
            return -1;
        }
    }
    public long CreateUser(String UserName,String Password,String Mobile,String Email) {
        ContentValues cv = new ContentValues();
        if(!CheckExistUser(Email)){
            cv.put("Username", UserName);
            cv.put("Password", Password);
            cv.put("Mobile", Mobile);
            cv.put("Email", Email);
            return ourDatabase.insert("UserTable", null, cv);
        } else {
            return 0;
        }
    }

    public boolean CheckExistUser(String email) {
        String searchQuery = "SELECT * FROM UserTable WHERE Email = '" + email +"'";
        Cursor cursor = ourDatabase.rawQuery(searchQuery, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public JSONArray GetDataChatAll() {
        String searchQuery;
        searchQuery = "SELECT * FROM notifications ORDER BY PrimID DESC";
        Cursor cursor = ourDatabase.rawQuery(searchQuery,null);
        return HelperObj.getInstance().GetJSONArray(cursor);
    }
    public JSONArray getUserTable() {
        String searchQuery;
        searchQuery = "SELECT * FROM UserTable";
       // searchQuery = "SELECT * FROM UserTable";
        Cursor cursor = ourDatabase.rawQuery(searchQuery,null);
        return HelperObj.getInstance().GetJSONArray(cursor);
    }
    public boolean CheckMessageExist(Integer MsgID,String MessageDate) {
        String searchQuery = "SELECT * FROM notifications WHERE MsgID = " + MsgID + " AND MessageDate = '" + MessageDate +"'";
        Cursor cursor = ourDatabase.rawQuery(searchQuery, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
    public long DeleteChatTableData() {
        long result;
        result = ourDatabase.delete("notifications", null, null);
        return result;
    }
    public long DeleteChatRowByID(Integer ID) {
        long result;
        result = ourDatabase.delete("notifications", "PrimID = ?", new String[]{String.valueOf(ID)});
        return result;
    }
    public long DeleteChatRow(String Message,String MessageType) {
        long result;
        result = ourDatabase.delete("notifications", "Message = ? AND MessageType = ?", new String[]{String.valueOf(Message),String.valueOf(MessageType)});
        return result;
    }
    public long DeleteChatDuplicates(String MessageType) {
        String searchQuery;
        searchQuery = "DELETE FROM notifications WHERE (PrimID NOT IN (SELECT MIN(PrimID) AS Expr1 FROM notifications AS Chat_1 GROUP BY Message)) AND (MessageType = ?)";
        ourDatabase.execSQL(searchQuery,new String[]{String.valueOf(MessageType)});
        return 0;
    }
}
