package com.superbia.superbia;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.superbia.superbia.database.DataBase;
import com.superbia.superbia.others.HelperObj;
import com.superbia.superbia.others.UserSession;

public class ActivityRegister extends AppCompatActivity {
    private TextView tvSignIn;
    private LinearLayout llSignInClick;
    private EditText etUsername,etMobileNumber,etPassword,etEmailAddress,etConfirmPassword;
    private Context context;
    private FragmentManager fragmentManager;
    private Button btnSignUP;
    private String PhoneNunmber="",Password="",Username="",EmailAddress="",ConfirmPassword="";
    private String name="",Email="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setUp();
        onClickEvents();
    }
    private void setUp() {
        try {
            context=getApplicationContext();
            fragmentManager=getSupportFragmentManager();
            //
            etUsername=(EditText)findViewById(R.id.etUsername);
            etMobileNumber=(EditText)findViewById(R.id.etMobileNumber);
            etEmailAddress=(EditText)findViewById(R.id.etEmailAddress);
            etPassword=(EditText)findViewById(R.id.etPassword);
            etConfirmPassword=(EditText)findViewById(R.id.etConfirmPassword);
            llSignInClick=(LinearLayout)findViewById(R.id.llSignInClick);
            tvSignIn=(TextView)findViewById(R.id.tvSignIn);
            btnSignUP=(Button)findViewById(R.id.btnSignUP);
            //
            name=getIntent().getStringExtra("Name");
            Email=getIntent().getStringExtra("Email");
            if(!name.isEmpty()){
                etUsername.setText(name);
            }
            if(!Email.isEmpty()){
                etEmailAddress.setText(Email);
            }
            //
            tvSignIn.setPaintFlags(tvSignIn.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void onClickEvents() {
        btnSignUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Username=etUsername.getText().toString();
                EmailAddress=etEmailAddress.getText().toString();
                PhoneNunmber=etMobileNumber.getText().toString();
                Password=etPassword.getText().toString();
                ConfirmPassword=etConfirmPassword.getText().toString();
                //
                if(Username.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter UserName.");
                } else if(PhoneNunmber.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter mobile number.");
                }else if(EmailAddress.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter Email.");
                } else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(EmailAddress).matches()){
                    HelperObj.getInstance().cusToast(context,"Please enter valid Email.");
                }else if(Password.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter password.");
                } else if(ConfirmPassword.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter confirm password.");
                }else if(!Password.equals(ConfirmPassword)){
                    HelperObj.getInstance().cusToast(context,"Please does not match.");
                } else {
                    DataBase dataBase=new DataBase(context);
                    dataBase.open();
                    Boolean isExist=dataBase.CheckExistUser(EmailAddress);
                    if(isExist){
                      HelperObj.getInstance().cusToast(context,"Your Already Have Account");
                    }else {
                        dataBase.CreateUser(Username,Password,PhoneNunmber,EmailAddress);
                        UserSession.getInstance(context).createUserSession(Username,EmailAddress,PhoneNunmber);
                        Intent intent =new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(intent);
                        dataBase.close();
                    }

                }
            }
        });
        llSignInClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(getApplicationContext(),ActivityLogin.class);
                startActivity(intent);
            }
        });

    }
}
