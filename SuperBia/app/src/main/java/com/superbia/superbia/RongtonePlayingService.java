package com.superbia.superbia;

import android.app.Notification;

;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;

import java.security.Provider;

public class RongtonePlayingService extends Service {
    MediaPlayer mediaPlayer;
    int startId=0;
    boolean isRunning;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public int onStartCommand(Intent intent, int flags, int startId){

        String sttus="";
        sttus = intent.getExtras().getString("extra");
        assert sttus!=null;
        if(sttus.equalsIgnoreCase("Yes")){
            startId=1;
        }else if(sttus.equalsIgnoreCase("No")){
            startId=0;
        }else {
            startId=0;
        }

        if(!this.isRunning&&startId==1){
            mediaPlayer= MediaPlayer.create(this,R.raw.dream);
            mediaPlayer.start();
            this.isRunning=true;
            this.startId=0;
            NotificationManager notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            Intent main=new Intent(this.getApplicationContext(),ActivityAlaram.class);
            PendingIntent p=PendingIntent.getActivity(this,0,main,0);
            Notification notification=new Notification.Builder(this)
                  .setContentTitle("Alarm is going on !")
                    .setContentText("click me !")
                    .setContentIntent(p)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .build();
            if (notificationManager != null) {
                notificationManager.notify(0,notification);
            }


        }else if(this.isRunning&&startId==0){
            mediaPlayer.stop();
            mediaPlayer.reset();
            this.isRunning=false;
            this.startId=0;

        }else if(!this.isRunning&&startId==0){
            this.isRunning=false;
            this.startId=0;

        }else if(this.isRunning&&startId==1){

            this.isRunning=true;
            this.startId=1;

        }else {

        }


        return START_NOT_STICKY;
    }
    @Override
    public void onDestroy(){
     super.onDestroy();
     this.isRunning=false;
    }
}
