package com.superbia.superbia.fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.superbia.superbia.R;
import com.superbia.superbia.others.HelperObj;
import com.superbia.superbia.others.Values;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragShopList extends Fragment {

    private FragmentManager fragmentManager;
    private Context context;
    private LinearLayout llList;
    private JSONArray jsonArrayData=null;
    private AssetManager assetManager;
    private InputStream is;
    private Bitmap bitmap;
    private JSONObject MainJSon=null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_frag_shop_list, container, false);
        context=getActivity();
        fragmentManager=getFragmentManager();
        llList=(LinearLayout)view.findViewById(R.id.llList);
        CalJson();
        return view;
    }
    private void CalJson() {
        try {
            assetManager = getActivity().getAssets();
            MainJSon=new JSONObject(loadJSONFromAsset());
            jsonArrayData=MainJSon.optJSONArray("Shops");
            setList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String loadJSONFromAsset() {
        String json = null;
        try {
            is = getActivity().getAssets().open("shopList.txt");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return json;
    }
    @SuppressLint({"InflateParams", "SetTextI18n"})
    public View getListItem(final Integer position, final JSONObject jsonObject){
        final TextView tv_Address,tv_Name;
        Button btnShow;

        View rowView = null;
        LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            rowView = inflater.inflate(R.layout.item_recipe_list, null);
            tv_Address = (TextView) rowView.findViewById(R.id.tv_Address);
            tv_Name = (TextView) rowView.findViewById(R.id.tv_Name);
            btnShow=(Button)rowView.findViewById(R.id.btnShow);
            //
            tv_Address.setText("Address : "+jsonObject.optString("Address"));
            tv_Name.setText("Shop Name : "+jsonObject.optString("ShopName"));
            btnShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        String url = jsonObject.optString("URL");
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            //
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowView;
    }
    public void setList(){
        try {
//            llList.addView(getListItem(jsonArrayData));

            if(jsonArrayData != null && jsonArrayData.length() > 0){
                llList.removeAllViews();
                for(int i = 0;i < jsonArrayData.length();i++){
                    llList.addView(getListItem(i,jsonArrayData.getJSONObject(i)));
                }
            } else {
                llList.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
