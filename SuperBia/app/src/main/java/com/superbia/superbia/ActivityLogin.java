package com.superbia.superbia;


import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.superbia.superbia.database.DataBase;
import com.superbia.superbia.others.HelperObj;
import com.superbia.superbia.others.MyExceptionHandler;
import com.superbia.superbia.others.UserSession;

import org.json.JSONArray;

public class ActivityLogin extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener {
    private TextView tvSignUp,tvForgotPassword;
    private LinearLayout llSignUpClick;
    private EditText etMobileNumber,etLoginPassword;
    private Context context;
    private FragmentManager fragmentManager;
    private Button btnLogin;
    private ImageButton ibGmail,ibFacebook;
    private String PhoneNunmber="",Password="";
    private JSONArray jsonArrayData=new JSONArray();
    public static int RC_SIGN_IN = 007;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    FragmentActivity parentActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, ActivityLogin.class));
        setContentView(R.layout.activity_login);

       setUp();
       onClickEvents();
       CallDatabase();
       //

    }
    public void CallDatabase() {
        try {
            DataBase dataBase=new DataBase(context);
            dataBase.open();
            jsonArrayData=dataBase.getUserTable();
            //dataBase.close();
            //

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void GmailSignIn() {
        try {
            if(mGoogleApiClient != null){
                mGoogleApiClient.clearDefaultAccountAndReconnect();
            }
            //
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
            mGoogleApiClient.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void setUp() {
        try {
            context=getApplicationContext();
            fragmentManager=getSupportFragmentManager();
            //
            etMobileNumber=(EditText)findViewById(R.id.etMobileNumber);
            etLoginPassword=(EditText)findViewById(R.id.etLoginPassword);
            llSignUpClick=(LinearLayout)findViewById(R.id.llSignUpClick);
            tvForgotPassword=(TextView)findViewById(R.id.tvForgotPassword);
            tvSignUp=(TextView)findViewById(R.id.tvSignup);
            btnLogin=(Button)findViewById(R.id.btnLogin);
            ibGmail=(ImageButton)findViewById(R.id.ibGmail);
            ibFacebook=(ImageButton)findViewById(R.id.ibFacebook);
            //
            tvSignUp.setPaintFlags(tvSignUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            try {
                gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();
                mGoogleApiClient = new GoogleApiClient.Builder(ActivityLogin.this)
                        .enableAutoManage(this, this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void onClickEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhoneNunmber=etMobileNumber.getText().toString();
                Password=etLoginPassword.getText().toString();
                //
                if(PhoneNunmber.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter Email address.");
                }else if(Password.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter password.");
                }else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(PhoneNunmber).matches()){
                    HelperObj.getInstance().cusToast(context,"Please enter valid Email.");
                } else {
                    try {
                        if(jsonArrayData!=null &&jsonArrayData.length()>0){
                            for(int i=0;i<jsonArrayData.length();i++){
                                if(jsonArrayData.getJSONObject(i).optString("Email").equalsIgnoreCase(PhoneNunmber)){
                                    if(jsonArrayData.getJSONObject(i).optString("Password").equalsIgnoreCase(Password)){
                                        UserSession.getInstance(context).createUserSession(jsonArrayData.getJSONObject(i).optString("Username"),jsonArrayData.getJSONObject(i).optString("Email"),jsonArrayData.getJSONObject(i).optString("Mobile"));
                                        Intent intent=new Intent(context,MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                }else {
                                   // HelperObj.getInstance().cusToast(context,"User Not Found");
                                }
                            }

                        }else {
                            HelperObj.getInstance().cusToast(context,"Please enter valid Email.And Password");
                        }
                        //dataBase.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,ActivityForgotPassword.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });
        llSignUpClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(getApplicationContext(),ActivityRegister.class);
                i.putExtra("Name","");
                i.putExtra("Email","");
                startActivity(i);
            }
        });
        ibGmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GmailSignIn();
            }
        });
        ibFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == RC_SIGN_IN){
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void handleSignInResult(GoogleSignInResult result){
        try {
            if (result.isSuccess()){
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct1 = result.getSignInAccount();
                assert acct1 != null;
                String first_name1= acct1.getDisplayName();
                String emailId1 = acct1.getEmail();
                //
                registerFacebook(first_name1,emailId1);
                /*etFullName.setText("");
                etLastName.setText("");
                etEmail.setText("");
                etFullName.setText(first_name);
                etEmail.setText(emailId);
                setLayout("RegisterNoReset");*/
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void registerFacebook(String first_name, String emailId) {
        Intent i=new Intent(getApplicationContext(),ActivityRegister.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("Name",first_name);
        i.putExtra("Email",emailId);
        startActivity(i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mGoogleApiClient.stopAutoManage(ActivityLogin.this);
            mGoogleApiClient.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
