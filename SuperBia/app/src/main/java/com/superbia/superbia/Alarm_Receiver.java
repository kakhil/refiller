package com.superbia.superbia;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class Alarm_Receiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        String s=intent.getExtras().getString("extra");
        Intent intent1=new Intent(context,RongtonePlayingService.class);
        intent1.putExtra("extra",s);
        context.startService(intent1);

    }
}
