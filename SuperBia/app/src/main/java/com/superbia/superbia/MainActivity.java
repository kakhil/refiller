package com.superbia.superbia;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.superbia.superbia.fragments.FragAccount;
import com.superbia.superbia.fragments.FragHome;
import com.superbia.superbia.fragments.FragLocations;
import com.superbia.superbia.fragments.FragShopList;
import com.superbia.superbia.others.MyExceptionHandler;
import com.superbia.superbia.others.Values;

import java.util.ArrayList;

import static com.superbia.superbia.others.Values.FragLocation;

public class MainActivity extends FragmentActivity {
    private FragmentManager fragmentManager;
    private Context context;
    private LinearLayout llBottomMenu;
    private ArrayList<Integer> menuImagesList,menuActiveImagesList;
    private ArrayList<String> menuTitleList;
    private Boolean isBackPressed = false,doublePressExit = false;
    private String MenuTYPE="1";
    String Message1="";
    private String Conditions="";
    private Activity actSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=getApplicationContext();
        actSplash=MainActivity.this;
        fragmentManager=getSupportFragmentManager();
        //
        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, MainActivity.class));
        //
        setUP();
    }
    private void setUP() {
        try {
            llBottomMenu=(LinearLayout)findViewById(R.id.llBottomMenu);
            menuImagesList = new ArrayList<>();
            menuImagesList.add(R.drawable.menu);
            menuImagesList.add(R.drawable.filter);
            menuImagesList.add(R.drawable.store);
            menuImagesList.add(R.drawable.account);
            //
            menuActiveImagesList = new ArrayList<>();
            menuActiveImagesList.add(R.drawable.menu);
            menuActiveImagesList.add(R.drawable.filter);
            menuActiveImagesList.add(R.drawable.store);
            menuActiveImagesList.add(R.drawable.account);
            //
            menuTitleList = new ArrayList<>();
            menuTitleList.add("Menu");
            menuTitleList.add("To find");
            menuTitleList.add("Shop");
            // menuTitleList.add("More");
            menuTitleList.add("My account");
            //
            createBottomMenu();
            ChangFragment(Values.FragHome,null);
            setActiveMenu(0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void createBottomMenu(){
        try {
            llBottomMenu.removeAllViews();
            for (int i = 0; i < menuImagesList.size(); i++) {
                llBottomMenu.addView(getMenuLayout(i));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @SuppressLint("InflateParams")
    public View getMenuLayout(final Integer position){
        ImageView ivItemMenu;
        TextView tvTitle;
        View rowView = null,viewMenu;
        LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            rowView = inflater.inflate(R.layout.item_top_menu, null);
            ivItemMenu = (ImageView) rowView.findViewById(R.id.ivItemMenu);
            tvTitle = (TextView) rowView.findViewById(R.id.tvTitle);
            viewMenu = (View) rowView.findViewById(R.id.viewMenu);
            //
            tvTitle.setText(menuTitleList.get(position));
            float scale = getResources().getDisplayMetrics().density;
            /*Integer singleItemWidth = (int)(getSingleMenuWidth()*scale);
            Integer ivItemWidth = singleItemWidth-50;
            if(ivItemWidth > 30){
                ivItemWidth = (int)(30*scale);
            }*/
            Integer ivItemWidth = (int)(30*scale);
            ivItemMenu.getLayoutParams().width = ivItemWidth;
            ivItemMenu.getLayoutParams().height = ivItemWidth;
            ivItemMenu.requestLayout();
            //
            ivItemMenu.setImageResource(menuImagesList.get(position));
            viewMenu.setBackgroundColor(context.getResources().getColor(R.color.transparent));
            //
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //setActiveMenu(position);
                    switch (position) {
                        case 0:
                            ChangFragment(Values.FragHome, null);
                            setActiveMenu(0);
                            break;
                        case 1:
                            checkPermission();
                            setActiveMenu(1);
                            break;
                        case 2:
                            ChangFragment(Values.FragShopList, null);
                            setActiveMenu(2);
                            break;
                        case 3:
                            ChangFragment(Values.FragAccount, null);
                            setActiveMenu(3);
                            break;
                    }
                }
            });
            Integer itemWidth = 0;
            Integer singleItemWidth = getSingleMenuWidth();
            if(singleItemWidth < 150){
                itemWidth = 150;
            } else {
                itemWidth = singleItemWidth;
            }
            LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(itemWidth,LinearLayout.LayoutParams.WRAP_CONTENT);
            rowView.setLayoutParams(llParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowView;
    }
    public void setActiveMenu(Integer position){
        try {
            for (int i = 0; i < menuImagesList.size(); i++) {
                LinearLayout llMenuItem = (LinearLayout) llBottomMenu.getChildAt(i);
                ImageView ivIcon = (ImageView) llMenuItem.getChildAt(0);
                TextView tvTitle = (TextView) llMenuItem.getChildAt(1);
                View viewMenu = (View) llMenuItem.getChildAt(2);
                if(position == i){
                    ivIcon.setImageResource(menuActiveImagesList.get(i));
                    tvTitle.setTextColor(context.getResources().getColor(R.color.green));
                    // viewMenu.setBackgroundColor(context.getResources().getColor(R.color.theme));
                } else {
                    ivIcon.setImageResource(menuImagesList.get(i));
                    tvTitle.setTextColor(context.getResources().getColor(R.color.test));
                    //viewMenu.setBackgroundColor(context.getResources().getColor(R.color.color999));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Integer getSingleMenuWidth(){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        int height = displaymetrics.heightPixels;
        return width/4;
    }
    public void ChangFragment(String tag,String[] parameter){
        try {
            try {
                //HelperObj.getInstance().HideKeyboard(viewGroup);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String title = context.getResources().getString(R.string.app_name);
            Fragment fragment = null;
            if(tag.equals(Values.FragHome)){
                fragment = new FragHome();
                setActiveMenu(0);
            }else if(tag.equals(Values.FragLocation)){
                fragment = new FragLocations();
                setActiveMenu(1);
            }
            else if(tag.equals(Values.FragShopList)){
                fragment = new FragShopList();
                setActiveMenu(2);
            }
            else if(tag.equals(Values.FragAccount)){
                fragment = new FragAccount();
                setActiveMenu(3);
            }
            if(fragment != null) {
                Bundle bundle = new Bundle();
                bundle.putStringArray("Parameter", parameter);
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment, tag).commit();
                //actionBar.setTitle(title);
                //
                Values.isWhichFrag = tag;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void checkPermission() {
        try {
            if (ActivityCompat.checkSelfPermission(actSplash, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(actSplash, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED  ) {
                ActivityCompat.requestPermissions(actSplash, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,android.Manifest.permission.ACCESS_COARSE_LOCATION}, 10);
            } else {
                ChangFragment(FragLocation, null);
//                Intent i=new Intent(MainActivity.this,ActivityMap.class);
//                startActivity(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (doublePressExit) {
            isBackPressed = true;
            doublePressExit = false;
            super.onBackPressed();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                finishAffinity();
                finish();
            }
            return;
        }
        doublePressExit = true;
        final Toast toast = Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT);
        toast.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doublePressExit = false;
                toast.cancel();
            }
        }, 1000);
    }

}





