package com.superbia.superbia;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.superbia.superbia.others.GPSTracker;
import com.superbia.superbia.others.HelperObj;
import com.superbia.superbia.others.PlaceArrayAdapter;
import com.superbia.superbia.others.Values;

import java.util.ArrayList;

public class ActivityMap extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks {
    private Context context;
    private FragmentManager fragmentManager;
    private GoogleMap googleMap;
    private GoogleApiClient mGoogleApiClient;
    private LatLng latLngOrg,DropLatLng;
    private Marker currLocationMarker;
    private  OnMapReadyCallback onMapReadyCallback;
    private ArrayList<Double> latList;
    private ArrayList<Double> Lonlist;
    private ArrayList<String> TypeList;
    private Button btnFountain,btnCoffee,btnWater;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private AutoCompleteTextView etSearch;
    private LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        //
        SetUp();
        onClickkEvents();
        //setCurrentLocation();
    }
    public void SetUp(){
        try {
            context=getApplicationContext();
            fragmentManager=getSupportFragmentManager();
            onMapReadyCallback = this;
            SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map1);
            mapFrag.getMapAsync(onMapReadyCallback);
            btnFountain=(Button)findViewById(R.id.btnFountain);
            btnWater=(Button)findViewById(R.id.btnWater);
            btnCoffee=(Button)findViewById(R.id.btnCoffee);

           // etSearch = (AutoCompleteTextView) findViewById(R.id.etSearch);
            latList=new ArrayList<>();
            TypeList=new ArrayList<>();
            //
            latList.add(-37.720712);
            latList.add(-37.720381);
            latList.add(-37.720993);
            latList.add(-37.721040);
            latList.add(-37.720743);
            //
            TypeList.add("Coffee");
            TypeList.add("Water");
            TypeList.add("Water");
            TypeList.add("Coffee");
            TypeList.add("Fountain");
            //
            Lonlist=new ArrayList<>();
            //
            Lonlist.add(145.048421);
            Lonlist.add(145.048678);
            Lonlist.add(145.048771);
            Lonlist.add(145.046740);
            Lonlist.add(145.048421);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void onClickkEvents() {
        btnFountain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleMap.clear();
                for(int i = 0 ; i < latList.size() ; i++) {
                    createMarker(TypeList.get(i),latList.get(i), Lonlist.get(i),"Fountain");
                }
            }
        });
        btnWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleMap.clear();
                for(int i = 0 ; i < latList.size() ; i++) {
                    createMarker(TypeList.get(i),latList.get(i), Lonlist.get(i),"Water");
                }
            }
        });
        btnCoffee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleMap.clear();
                for(int i = 0 ; i < latList.size() ; i++) {
                    createMarker(TypeList.get(i),latList.get(i), Lonlist.get(i),"Coffee");
                }
            }
        });
    }
    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        try {
            if (googleMap != null) {
                if (ActivityCompat.checkSelfPermission(ActivityMap.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ActivityMap.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    googleMap.setMyLocationEnabled(false);
                }
                buildGoogleApiClient();
            } else {
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    protected synchronized void buildGoogleApiClient() {
        try {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .build();
            mGoogleApiClient.connect();
            //
//            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//                @Override
//                public void onMapClick(LatLng latLng) {
//                    //HelperObj.getInstance().cusToast(context,""+latLng.latitude);
//                    //setMarker(latLng);
//                }
//            });
            //
            onGPS();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected Marker createMarker(String Type ,double latitude, double longitude,String condition) {
        Marker marker = null;
        if(condition.equalsIgnoreCase("Coffee")){
            if(Type.equalsIgnoreCase("Coffee")) {
                marker = googleMap.addMarker( new MarkerOptions()
                    .position( new LatLng( latitude, longitude ) )
                    .anchor( 0.4f, 0.4f )
                    .title( Type )
                    .snippet( "" )
                    .icon( BitmapDescriptorFactory.fromResource( R.drawable.cafe ) ) );
            }

        }else if(condition.equalsIgnoreCase("Water")){
            if(Type.equalsIgnoreCase("Water")){
                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .anchor(0.4f, 0.4f)
                        .title(Type)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.water)));
            }

        }else if(condition.equalsIgnoreCase("Fountain")){
            if(Type.equalsIgnoreCase("Fountain")){
                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .anchor(0.4f, 0.4f)
                        .title(Type)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.fountain)));
            }

        }else {
            if(Type.equalsIgnoreCase("Coffee")){
                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .anchor(0.4f, 0.4f)
                        .title(Type)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.cafe)));
            }else  if(Type.equalsIgnoreCase("Water")){
                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .anchor(0.4f, 0.4f)
                        .title(Type)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.water)));

            }else  if(Type.equalsIgnoreCase("Fountain")){
                marker= googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .anchor(0.4f, 0.4f)
                        .title(Type)
                        .snippet("")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.fountain)));
            }
        }

        return marker;
    }
    @Override
    public void onConnected(Bundle bundle) {
        try {
            Location mLastLocation = null;
            if (ActivityCompat.checkSelfPermission(ActivityMap.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ActivityMap.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);
            }
            if (mLastLocation != null) {
                //place marker at current position
                googleMap.clear();
                latLngOrg = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            } else {
                latLngOrg = new LatLng(Values.latitude, Values.longitude);
            }
            // mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);

//            setCurrentLocation();
//            setMarker(latLngOrg);
            googleMap.clear();
            for(int i = 0 ; i < latList.size() ; i++) {
                createMarker(TypeList.get(i),latList.get(i), Lonlist.get(i),"All");
            }
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLngOrg).zoom(11).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onConnectionSuspended(int i) {
        try {
            // mPlaceArrayAdapter.setGoogleApiClient(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    protected void setCurrentLocation() {
        try {
            GPSTracker gps = new GPSTracker(context);
            if(gps.canGetLocation()) {
                double latitudeCurrent = gps.getLatitude();
                double longitudeCurrent = gps.getLongitude();
                if(latitudeCurrent > 0 && longitudeCurrent > 0){
                    latLngOrg = new LatLng(latitudeCurrent, longitudeCurrent);
                    setMarker(latLngOrg);
                }
            } else {
                latLngOrg = new LatLng(Values.latitude, Values.longitude);
                setMarker(latLngOrg);
            }

            //etSearch.setText("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  void setMarker(LatLng latLng){
        try {
            double latitudeOrg=0,longitudeOrg=0;
            latitudeOrg = latLng.latitude;
            longitudeOrg = latLng.longitude;
            if(latitudeOrg > 0 && longitudeOrg > 0 ){
                googleMap.clear();
                //String cityName = HelperObj.getInstance().getLocationName(context, latitudeOrg, longitudeOrg);
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("curLoc");
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).draggable(true);
                currLocationMarker = googleMap.addMarker(markerOptions);
                //
                latLngOrg = latLng;
                float zoomLevel = googleMap.getCameraPosition().zoom;
                if(zoomLevel < 11){
                    zoomLevel = 11;
                }
                for(int i = 0 ; i < latList.size() ; i++) {
                    createMarker(TypeList.get(i),latList.get(i), Lonlist.get(i),"All");
                }
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLngOrg).zoom(zoomLevel).build();
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            } else {
                HelperObj.getInstance().cusToast(context,"Lat,Lng not found. Select valid location");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onGPS(){
        LocationRequest mLocationRequest = new LocationRequest();
        // Check the location settings of the user and create the callback to react to the different possibilities
        LocationSettingsRequest.Builder locationSettingsRequestBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, locationSettingsRequestBuilder.build());
        result.setResultCallback(mResultCallbackFromSettings);
    }
    // The callback for the management of the user settings regarding location
    private ResultCallback<LocationSettingsResult> mResultCallbackFromSettings = new ResultCallback<LocationSettingsResult>() {
        @Override
        public void onResult(LocationSettingsResult result) {
            final Status status = result.getStatus();
            //final LocationSettingsStates locationSettingsStates = result.getLocationSettingsStates();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(ActivityMap.this,0);
                    } catch (Exception e) {
                        // Ignore the error.
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Log.e("TAG", "Settings change unavailable. We have no way to fix the settings so we won't show the dialog.");
                    break;
            }
        }
    };
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(intent);
        switch (requestCode) {
            case 0:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        if (mGoogleApiClient.isConnected()/* && userMarker == null*/) {
                            // HelperObj.getInstance().Loader(context,fragmentManager,true,"Getting location...");
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        // HelperObj.getInstance().Loader(context,fragmentManager,false,null);
                                       // setCurrentLocation();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, 5000);
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        break;
                    default:
                        break;
                }
                break;
        }
    }

}
