package com.superbia.superbia;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class ActivityAlaram extends FragmentActivity {
    private Context context;
    private FragmentManager fragmentManager;
    private ImageView ib_back;
    private TextView tv_Alarm;
    private Button btn_setAlarm,btn_CancelAlarm;
    private TimePicker time_picker;
    private AlarmManager alarmManager;
    private Calendar calendar;
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alaram);
        context=getApplicationContext();
        fragmentManager=getSupportFragmentManager();
        //
        alarmManager=(AlarmManager)getSystemService(ALARM_SERVICE);
        ib_back=(ImageView)findViewById(R.id.ib_back);
        tv_Alarm=(TextView)findViewById(R.id.tv_Alaram);
        btn_setAlarm=(Button)findViewById(R.id.btn_setAlarm);
        btn_CancelAlarm=(Button)findViewById(R.id.btn_CancelAlarm);
        time_picker=(TimePicker)findViewById(R.id.time_picker);
        //
         calendar=Calendar.getInstance();
        final Intent myIntent=new Intent(context,Alarm_Receiver.class);
        //
        ib_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btn_setAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    calendar.set(Calendar.HOUR_OF_DAY,time_picker.getHour());
                    calendar.set(Calendar.MINUTE,time_picker.getMinute());
                    String Hour= String.valueOf(time_picker.getHour());
                    String Minute= String.valueOf(time_picker.getMinute());
                    int hour=time_picker.getHour();
                    int minute=time_picker.getMinute();
                    if(hour>12){
                        Hour= String.valueOf(hour-12);
                    }
                    if(minute<10){
                        Minute= "0"+String.valueOf(minute);
                    }
                    SetAlaram("Alarm Set On : "+Hour+":"+Minute);
                    myIntent.putExtra("extra","Yes");
                    pendingIntent= PendingIntent.getBroadcast(ActivityAlaram.this,0,myIntent,PendingIntent.FLAG_UPDATE_CURRENT);
                    alarmManager.set(alarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);
                }
            }


        });
        btn_CancelAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SetAlaram("Alarm Off !");
                    Intent intent = new Intent(context, Alarm_Receiver.class);
                    intent.putExtra("extra","No");
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1253, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.cancel(pendingIntent);
                    sendBroadcast(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    private void SetAlaram(String s) {
        tv_Alarm.setText(s);
    }
}
