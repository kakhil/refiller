package com.superbia.superbia.fragments;



import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.awareness.fence.DetectedActivityFence;
import com.superbia.superbia.ActivitFeedBack;
import com.superbia.superbia.ActivityAbout;
import com.superbia.superbia.ActivityContact;
import com.superbia.superbia.ActivityFunStaff;
import com.superbia.superbia.ActivityHowToUse;
import com.superbia.superbia.ActivityMap;
import com.superbia.superbia.ActivityPrivacy;
import com.superbia.superbia.ActivityTerms;
import com.superbia.superbia.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragHome extends Fragment {
    private Button btnHowtoUse,btnABOUT_REFILLR,btnFeedBack,btnFunStuff,btnHelp,btnPRIVACY,btnTERMSOFUSE;
    private Context context;
    private FragmentManager fragmentManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.frag_home, container, false);
        context=getActivity();
        fragmentManager=getFragmentManager();
        //
        btnHowtoUse=(Button)view.findViewById(R.id.btnHowtoUse);
        btnABOUT_REFILLR=(Button)view.findViewById(R.id.btnABOUT_REFILLR);
        btnFeedBack=(Button)view.findViewById(R.id.btnFeedBack);
        btnFunStuff=(Button)view.findViewById(R.id.btnFunStuff);
        btnHelp=(Button)view.findViewById(R.id.btnHelp);
        btnPRIVACY=(Button)view.findViewById(R.id.btnPRIVACY);
        btnTERMSOFUSE=(Button)view.findViewById(R.id.btnTERMSOFUSE);

        //

        btnHowtoUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,ActivityHowToUse.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });
        btnABOUT_REFILLR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,ActivityAbout.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });
        btnFeedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,ActivitFeedBack.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });
        btnFunStuff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,ActivityFunStaff.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });
        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,ActivityContact.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });
        btnPRIVACY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,ActivityPrivacy.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });
        btnTERMSOFUSE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,ActivityTerms.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });
        return view;
    }

}
