package com.superbia.superbia;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.superbia.superbia.others.HelperObj;

public class ActivityOtp extends AppCompatActivity {
    private TextView tvResend;
    private Button btnSubmit;
    private EditText etOtp;
    private Context context;
    private FragmentManager fragmentManager;
    private String OTP="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        setUp();
        onClickEvents();
    }

    private void setUp() {
        context=getApplicationContext();
        fragmentManager=getFragmentManager();
        //
        etOtp=(EditText)findViewById(R.id.etOTP);
        tvResend=(TextView) findViewById(R.id.tvResend);
        btnSubmit=(Button) findViewById(R.id.btnSubmit);
        //
        tvResend.setPaintFlags(tvResend.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }
    private void onClickEvents() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OTP=etOtp.getText().toString();
                if(OTP.isEmpty()){
                    HelperObj.getInstance().cusToast(context,"Please enter OTP.");
                }else {
                    HelperObj.getInstance().cusToast(context,"Thank you .");
                }
            }
        });
    }
}
